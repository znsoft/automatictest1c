﻿
////////////////////////////////////////////////////////////////////////////////
// ОБРАБОТЧИКИ СОБЫТИЙ ФОРМЫ И ЭЛЕМЕНТОВ ДИАЛОГА

Процедура КнопкаВыполнитьНажатие(Кнопка)
	
	// Строки, которые нужно добавить уже отмечены.
	
	// Для отмеченных строк нужно составить таблицу ссылок и предложить ее заполнить
	Таблица = ИСТЗ_СоздатьСтруктуруТаблицыСсылок();
	НомерИтерации = 1;
	ИСТЗ_ИтерационноеЗаполнениеТаблицыСсылок(Таблица, ДобавляемыйСценарийТеста, Истина, Неопределено, 0, НомерИтерации, "Отметка");
	
	//Заполнение сформированной таблицы структурой идентификации из скопированной из файла таблицы ссылок
	ЕстьРазлияСсылок = Ложь;
	Для каждого СтрокаТаблицы Из Таблица Цикл
		Попытка
			Объект = СтрокаТаблицы.Ссылка.ПолучитьОбъект();
		Исключение
			Объект = Неопределено;
		КонецПопытки;
		
		Если Объект <> Неопределено Тогда
			// Эта ссылка есть в эталонной базе
			СтрокаТаблицы.СоответствиеСсылок = СтрокаТаблицы.Ссылка;
		Иначе
			// Ссылки нет в эталонной базе
			ЕстьРазлияСсылок = Истина;
			СтрокаВИсходнойТаблицеСсылок = ТаблицаДобавляемыхСсылок.Найти(СтрокаТаблицы.Ссылка, "Ссылка");
			Если СтрокаВИсходнойТаблицеСсылок <> Неопределено Тогда
				СтрокаТаблицы.СтруктураИдентификации = СтрокаВИсходнойТаблицеСсылок.СтруктураИдентификации;
			КонецЕсли;
			СтрокаТаблицы.СоответствиеСсылок = Неопределено;
		КонецЕсли;
	КонецЦикла;
	
	Если ЕстьРазлияСсылок Тогда
		ФормаСсылок = ПолучитьФорму("ФормаСсылок", ЭтаФорма);
		
		// Любое изменение должно выполняться в этой таблице
		ФормаСсылок.ИсходнаяТаблица = Таблица;
		ФормаСсылок.ШагиОсновногоСценария = Ложь;
		// Пока ссылки не подобраны добавлять шаги сценария нельзя
		// Если ссылки не подобраны сразу, пользователь должен отказаться от подбора осознанно.
		ФормаСсылок.ОткрытьМодально();
	КонецЕсли;
	
	// Добавление строк в сценарий с одновременной подменой ссылок
	ИСТЗ_РекурсивноСкопироватьШагиВСценарийТеста(ДобавляемыйСценарийТеста, СценарийТеста, Таблица);
	ИСТЗ_ПронумероватьШаги();
	Форма = ПолучитьФорму("Форма");
	Форма.Модифицированность = Истина;
	Закрыть();
	
КонецПроцедуры

Процедура ДобавляемыйСценарийТестаПриВыводеСтроки(Элемент, ОформлениеСтроки, ДанныеСтроки)
	
	Если мСписокТиповШагов.НайтиПоЗначению(ДанныеСтроки.ТипШага) <> Неопределено Тогда
		ОформлениеСтроки.Ячейки.ТипШага.Значение = мСписокТиповШагов.НайтиПоЗначению(ДанныеСтроки.ТипШага).Представление;
	КонецЕсли;
	
	ОформлениеСтроки.Ячейки.Отметка.ОтображатьФлажок = Истина;
	ОформлениеСтроки.Ячейки.Отметка.Флажок = ДанныеСтроки.Отметка;
	
	ОформлениеСтроки.Ячейки.Наименование.ОтображатьФлажок = Истина;
	ОформлениеСтроки.Ячейки.Наименование.Флажок = ДанныеСтроки.Активность;
	
	Если ДанныеСтроки.ТипШага = "АвтоГруппа"
		ИЛИ ДанныеСтроки.ТипШага = "Группа" Тогда
		
		ОформлениеСтроки.Шрифт = Новый Шрифт(ОформлениеСтроки.Шрифт, , , Истина);
		
	КонецЕсли;
	
	// Картинка в поле ВыполнитьВРучную
	Если    ДанныеСтроки.ТипШага = "АвтоГруппа"
		ИЛИ ДанныеСтроки.ТипШага = "Группа"
		ИЛИ ДанныеСтроки.ТипШага = "Комментарий" Тогда
		
		ОформлениеСтроки.Ячейки.ВыполнитьВРучную.ОтображатьКартинку = Ложь;
		ОформлениеСтроки.Ячейки.ВыполнитьВРучную.ИндексКартинки     = 1;
		
	ИначеЕсли ДанныеСтроки.ВыполнитьВручную Тогда
		
		ОформлениеСтроки.Ячейки.ВыполнитьВРучную.ОтображатьКартинку = Истина;
		ОформлениеСтроки.Ячейки.ВыполнитьВРучную.ИндексКартинки     = 1;
		
	Иначе
		
		ОформлениеСтроки.Ячейки.ВыполнитьВРучную.ОтображатьКартинку = Истина;
		ОформлениеСтроки.Ячейки.ВыполнитьВРучную.ИндексКартинки     = 0;
		
	КонецЕсли;
	
	// Картинка в поле Картинка
	Если ДанныеСтроки.ТипШага = "Группа" Тогда
		Если ТипЗнч(ДанныеСтроки.Значение) = Тип("Булево") И ДанныеСтроки.Значение Тогда
			ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
			ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 24;
		КонецЕсли;
		
	ИначеЕсли ДанныеСтроки.ТипШага = "АвтоГруппа" Тогда
		
		Если ДанныеСтроки.ОбСсылка <> Неопределено Тогда
		
			Если ДанныеСтроки.ТипМетаданных = "Справочники" Тогда
				
				ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
				
				Если ДанныеСтроки.ОбСсылка = Неопределено Тогда
					Объект = Неопределено;
				Иначе
					Ссылка = ДанныеСтроки.ОбСсылка;
					Попытка
						Объект = Ссылка.ПолучитьОбъект();
					Исключение
						Объект = Неопределено;
					КонецПопытки;
				КонецЕсли;
				
				Если Объект = Неопределено Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 1;
				ИначеЕсли Объект.ПометкаУдаления Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 11;
				Иначе
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 8;
				КонецЕсли;
				
			ИначеЕсли ДанныеСтроки.ТипМетаданных = "ПланыСчетов" Тогда
				
				ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
				
				Если ДанныеСтроки.ОбСсылка = Неопределено Тогда
					Объект = Неопределено;
				Иначе
					Ссылка = ДанныеСтроки.ОбСсылка;
					Попытка
						Объект = Ссылка.ПолучитьОбъект();
					Исключение
						Объект = Неопределено;
					КонецПопытки;
				КонецЕсли;
				
				Если Объект = Неопределено Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 20;
				ИначеЕсли Объект.ПометкаУдаления Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 22;
				Иначе
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 21;
				КонецЕсли;
				
			ИначеЕсли ДанныеСтроки.ТипМетаданных = "ПланыВидовРасчета" Тогда
				
				ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
				
				Если ДанныеСтроки.ОбСсылка = Неопределено Тогда
					Объект = Неопределено;
				Иначе
					Ссылка = ДанныеСтроки.ОбСсылка;
					Попытка
						Объект = Ссылка.ПолучитьОбъект();
					Исключение
						Объект = Неопределено;
					КонецПопытки;
				КонецЕсли;
				
				Если Объект = Неопределено Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 14;
				ИначеЕсли Объект.ПометкаУдаления Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 16;
				Иначе
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 15;
				КонецЕсли;
				
			ИначеЕсли ДанныеСтроки.ТипМетаданных = "ПланыВидовХарактеристик" Тогда
				
				ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
				
				Если ДанныеСтроки.ОбСсылка = Неопределено Тогда
					Объект = Неопределено;
				Иначе
					Ссылка = ДанныеСтроки.ОбСсылка;
					Попытка
						Объект = Ссылка.ПолучитьОбъект();
					Исключение
						Объект = Неопределено;
					КонецПопытки;
				КонецЕсли;
				
				Если Объект = Неопределено Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 17;
				ИначеЕсли Объект.ПометкаУдаления Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 19;
				Иначе
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 18;
				КонецЕсли;
				
			ИначеЕсли ДанныеСтроки.ТипМетаданных = "Документы" Тогда
				
				ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
				
				Если ДанныеСтроки.ОбСсылка = Неопределено Тогда
					Объект = Неопределено;
				Иначе
					Ссылка = ДанныеСтроки.ОбСсылка;
					Попытка
						Объект = Ссылка.ПолучитьОбъект();
					Исключение
						Объект = Неопределено;
					КонецПопытки;
				КонецЕсли;
				
				Если Объект = Неопределено Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 0;
				ИначеЕсли Объект.ПометкаУдаления Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 10;
				ИначеЕсли Объект.Проведен Тогда
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 12;
				Иначе
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 7;
				КонецЕсли;
				
			ИначеЕсли ДанныеСтроки.ТипМетаданных = "РегистрыСведений" Тогда
				
				ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
				
				Попытка
					НаборЗаписей = РегистрыСведений[ДанныеСтроки.ИмяМетаданных].СоздатьНаборЗаписей();
							
					Для каждого СтрокаОтбора Из ДанныеСтроки.ОбСсылка Цикл
						НаборЗаписей.Отбор[СтрокаОтбора.Ключ].Установить(СтрокаОтбора.Значение);
					КонецЦикла;
					
					НаборЗаписей.Прочитать();
					Если НаборЗаписей.Количество() > 0 Тогда
						ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 9;
					Иначе
						ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 2;
					КонецЕсли;
				Исключение
					ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 2;
				КонецПопытки;
				
			Иначе
				ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
				ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 23;
				
			КонецЕсли;
		Иначе
			ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
			ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 23;
			
		КонецЕсли;
		
	ИначеЕсли ДанныеСтроки.ТипШага = "Сравнить" Тогда
		
		ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
		Если ДанныеСтроки.Значение = Неопределено Тогда
			ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 4;
		Иначе
			ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки = 5;
		КонецЕсли;
		
	ИначеЕсли ДанныеСтроки.ТипШага = "СравнитьСЭталоном"
		ИЛИ   ДанныеСтроки.ТипШага = "СравнитьДвижения"
		ИЛИ   ДанныеСтроки.ТипШага = "ВыполнитьЗапрос" Тогда
		
		ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
		ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки     = 4;
		
	ИначеЕсли ДанныеСтроки.ТипШага = "ВыполнитьВРучную" Тогда
		
		ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
		ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки     = 6;
		
	ИначеЕсли ДанныеСтроки.ТипШага = "Комментарий" Тогда
		
		ОформлениеСтроки.Ячейки.Картинка.ОтображатьКартинку = Истина;
		ОформлениеСтроки.Ячейки.Картинка.ИндексКартинки     = 13;
		
	КонецЕсли;
	
	// Цветовое оформление строки
	Если ДанныеСтроки.Активность = 0 Тогда
		ОформлениеСтроки.ЦветТекста = WebЦвета.СветлоСерый;
	Иначе
		Если ДанныеСтроки.Цвет <> Неопределено Тогда
			Если ДанныеСтроки.Цвет.Вид = ВидЦвета.Абсолютный Тогда
				ОформлениеСтроки.ЦветТекста = Новый Цвет(ДанныеСтроки.Цвет.Красный, ДанныеСтроки.Цвет.Зеленый, ДанныеСтроки.Цвет.Синий);
			Иначе
				ОформлениеСтроки.ЦветТекста = ДанныеСтроки.Цвет;
			КонецЕсли;
			
		КонецЕсли;
	КонецЕсли;
	
КонецПроцедуры

Процедура ДобавляемыйСценарийТестаПриАктивизацииСтроки(Элемент)
	
	Если Элемент.ТекущаяСтрока = Неопределено Тогда
		ОписаниеШага = "";
	Иначе
		ОписаниеШага = ИСТЗ_СформироватьОписание(Элемент.ТекущаяСтрока);
	КонецЕсли;
	
КонецПроцедуры

Процедура ДобавляемыйСценарийТестаПриИзмененииФлажка(Элемент, Колонка)
	
	Если Колонка.Имя = "Отметка" Тогда
		
		Если НЕ ИСТЗ_ЭтоГрупповойУзелСценария(Элемент.ТекущиеДанные) Тогда
		
			Если Элемент.ТекущиеДанные.Отметка = 0 Тогда 
				Элемент.ТекущиеДанные.Отметка = 1;
			Иначе
				Элемент.ТекущиеДанные.Отметка = 0;
			КонецЕсли;
			
		Иначе
			
			Если Элемент.ТекущиеДанные.Отметка = 0 Тогда 
				Элемент.ТекущиеДанные.Отметка = 1;
			Иначе
				Элемент.ТекущиеДанные.Отметка = 0;
			КонецЕсли;
			
			ИСТЗ_УстановитьФлагиПодчиненныхСтрок(Элемент.ТекущаяСтрока, Элемент.ТекущиеДанные.Отметка, "Отметка");
			//Оповестить("ПроверитьФлажокАктивности");
		КонецЕсли;
		
		ИСТЗ_УстановитьФлагиРодительскихСтрок(Элемент.ТекущиеДанные.Родитель, , , "Отметка");
		
	КонецЕсли;
	
КонецПроцедуры

////////////////////////////////////////////////////////////////////////////////
// ВСПОМОГАТЕЛЬНЫЕ ПРОЦЕДУРЫ И ФУНКЦИИ

Процедура ИСТЗ_РекурсивноСкопироватьШагиВСценарийТеста(ВеткаИсточник, ВеткаПриемник, ТаблицаПодменыСсылок)
	
	Для каждого СтрокаИсточник Из ВеткаИсточник.Строки Цикл
		
		Если СтрокаИсточник.Отметка > 0 Тогда
			
			ИСТЗ_ЗаменитьСсылкиВШаге(СтрокаИсточник, ТаблицаПодменыСсылок);
			
			СтрокаПриемник = ВеткаПриемник.Строки.Добавить();
			Для каждого Колонка Из СценарийТеста.Колонки Цикл
				СтрокаПриемник[Колонка.Имя] = СтрокаИсточник[Колонка.Имя];
			КонецЦикла;
			
			ИСТЗ_РекурсивноСкопироватьШагиВСценарийТеста(СтрокаИсточник, СтрокаПриемник, ТаблицаПодменыСсылок);
			
		КонецЕсли;
		
	КонецЦикла;
	
КонецПроцедуры

